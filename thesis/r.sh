#!/bin/bash

#if [[ "$1" == "clean" ]]; then
#    rm -f {bachelor,master}-thesis{,-legacy}.{bib,aux,log,bbl,bcf,blg,run.xml,toc,tct,pdf,out}
#else
#    for i in bachelor; do
#        xelatex $i-thesis
#        biber   $i-thesis
#        xelatex $i-thesis
#        xelatex $i-thesis
#    done
#
#    #for i in bachelor; do
#    #    pdflatex $i-thesis-legacy
#    #    biber    $i-thesis-legacy
#    #    pdflatex $i-thesis-legacy
#    #    pdflatex $i-thesis-legacy
#    #done
#
#    rm -f {bachelor,master}-thesis{,-legacy}.{bib,aux,log,bbl,bcf,blg,run.xml,toc,tct,out}
#fi
xelatex bachelor-thesis
biber    bachelor-thesis
xelatex -halt-on-error bachelor-thesis
xelatex -halt-on-error bachelor-thesis

rm bachelor-thesis.{aux,log,bbl,bcf,blg,run.xml,toc,tct}

pdftk bachelor-thesis.pdf cat 1-2 output build/title-page.pdf
pdftk bachelor-thesis.pdf cat 3 output build/specification.pdf
pdftk bachelor-thesis.pdf cat 4-5 output build/annotation.pdf
pdftk bachelor-thesis.pdf cat 6-end output build/thesis.pdf
