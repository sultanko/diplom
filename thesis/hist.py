import matplotlib.pyplot as plt
import json
import numpy as np
from scipy import stats

with open('data/nt_from_run_server_300.json') as f:
    data = json.load(f)

lst_names = ["NT-7", "NT-8", "NT-10"]

for nt_name in lst_names:
    nt_data2 = data[2][nt_name][0:300]
    #nt_data_range = (310, 390)
    nt_data_range = (200, 2000)
    nt_data = sorted([x for x in nt_data2 if x > nt_data_range[0] and x < nt_data_range[1]])

    nt_mean = np.mean(nt_data)
    nt_std = np.std(nt_data)
    t_crit = stats.t.interval(0.99, len(nt_data))
    nt_delta = t_crit[1] * stats.sem(nt_data)
    nt_range = np.linspace(nt_data_range[0], nt_data_range[1], num=len(nt_data))

    print (nt_name)
    print ('Mean: {}'.format(nt_mean))
    print ('Deviation: {}'.format(nt_std))
    print ('Error: {}'.format(nt_delta))
    print ('Rel er: {}'.format(nt_delta / nt_mean))

    fig, ax = plt.subplots(1, 1)
    n, bins, patches = ax.hist(nt_data, bins=20, facecolor='green', density=True, histtype='stepfilled', alpha=0.2)
    ax.plot(nt_range, stats.norm.pdf(nt_range, nt_mean, nt_std), 'r-', lw=5, alpha=0.6, label='norm pdf')


    plt.xlabel('мс')
    plt.ylabel('Частота')
    #plt.title('Гистограмма значений NT-8')

    #plt.show()
    #plt.savefig("temp/plot.png", bbox_inches='tight')

